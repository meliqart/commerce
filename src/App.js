import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom"
import Listing from "./pages/Listing";

function App(props) {
    return (
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<Listing/>}/>
                </Routes>
            </BrowserRouter>
    );
}

export default App;
