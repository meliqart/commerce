import React from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";
import Main from "../components/Main";

function Listing(props) {
    return (
        <div>
            <Header/>
            <Main/>
            <Footer/>
        </div>
    );
}

export default Listing;