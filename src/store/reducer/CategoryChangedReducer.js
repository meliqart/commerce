import {CATEGORY_VALUE_CHANGED} from "../action/CategoryChanged";


const initialState = {
    id: [],
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case CATEGORY_VALUE_CHANGED : {

            return {
                ...state,
                id: action.payload.CategoryId
            }
        }

        default: {
            return state
        }
    }

}
