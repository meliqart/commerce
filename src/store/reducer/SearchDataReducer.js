import {GET_MESSAGE_DATA} from "../action/SearchDataAction";

const initialState = {
    searchData: "",
};
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case GET_MESSAGE_DATA : {

            return {
                ...state,
                searchData: action.payload.data
            }
        }
        default: {
            return state
        }
    }

}
