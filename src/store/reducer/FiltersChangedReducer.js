import {FILTER_CHANGED, RATING_CHANGED} from "../action/FiltersChenged";



const initialState = {
    filters: [],
    rating:[]
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FILTER_CHANGED : {

            return {
                ...state,
                filters: action.payload.filter
            }
        }
        case RATING_CHANGED : {

            return {
                ...state,
                rating: action.payload.rating
            }
        }

        default: {
            return state
        }
    }

}
