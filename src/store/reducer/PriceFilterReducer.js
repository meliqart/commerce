import {GET_MIN_MAX_VALUE} from "../action/PriceCategory";


const initialState = {
    data: [],
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case GET_MIN_MAX_VALUE : {
            return {
                ...state,
                data: action.payload.data
            }
        }

        default: {
            return state
        }
    }

}