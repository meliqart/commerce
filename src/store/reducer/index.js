import React from 'react';
import {combineReducers} from "redux";
import searchData from "./SearchDataReducer"
import categoryId from "./CategoryChangedReducer"
import priceData from "./PriceFilterReducer"
import filters from "./FiltersChangedReducer"

export default combineReducers({
        searchData,
        categoryId,
        priceData,
        filters
});

