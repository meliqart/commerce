export const GET_MESSAGE_DATA = "GET_MESSAGE_DATA"

export function getMessageData(data) {
    return {
        type: GET_MESSAGE_DATA,
        payload: {
            data
        }
    }
}

