export const CATEGORY_VALUE_CHANGED = "CATEGORY_VALUE_CHANGED";

export const categoryChanged = (CategoryId) => {
    return {
        type: CATEGORY_VALUE_CHANGED,
        payload: {
            CategoryId:[...CategoryId]
        }
    }
}
