export const GET_MIN_MAX_VALUE = "GET_MIN_MAX_VALUE"

export function getPriceData(data) {
    return {
        type: GET_MIN_MAX_VALUE,
        payload: {
            data: data
        }
    }
}
