export const FILTER_CHANGED = "FILTER_CHANGED";
export const RATING_CHANGED = "RATING_CHANGED"

export const filtersChanged = (data) => {
    return {
        type: FILTER_CHANGED,
        payload: {
            filter:data
        }
    }
}
export const ratingChanged = (data) => {
    return {
        type: RATING_CHANGED,
        payload: {
            rating:data
        }
    }
}
