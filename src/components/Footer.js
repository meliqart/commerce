import React, {useState} from 'react';
import _ from "lodash"
import {Link} from "react-router-dom";
import {BsFacebook} from "react-icons/bs";
import {AiFillInstagram} from "react-icons/ai"
import {AiFillYoutube} from "react-icons/ai";
import {CgComedyCentral} from "react-icons/cg";
import data from "../data/footerMenu.json"

function Footer(props) {

    return (
        <div className="footer">
            <div className="container">
                <div className="footer_data">
                    <div className="sign_up">
                        <h2>Sign up for news</h2>
                        <p>Keep up to date with the latest product<br/>
                            and news. Find out more about our<br/>
                            brands and get special promo codes.</p>
                        <form action="">
                            <input className="footer_input" type='text' placeholder="Your e-mail address"/><br/>
                            <button className="footer_button"><span>Sign up for newsletter</span></button>
                        </form>
                    </div>
                    {_.map(data, (item => {
                        return <div  key={item.id} className={item.className}>
                            <h2>{item.name}</h2>
                            {item.data.map((i) => {
                                return <div key={i.id}><Link className="footer_link" to="#"><p>{i.value}</p></Link>
                                </div>
                            })}
                        </div>
                    }))
                    }
                </div>
                <div className="footer_social">
                    <div className="left_block">
                        <CgComedyCentral/>
                        <span>2021 Divante S.A.</span>
                    </div>
                    <div className="right_block">
                        <BsFacebook className="footer_icon"/>
                        <AiFillInstagram className="footer_icon"/>
                        <AiFillYoutube className="footer_icon"/>
                        <img src="logo/vector3.png" alt="logo"/>
                        <img style={{width: 180, height: 15}} src="logo/commercebooster.png" alt="logo"/>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Footer;