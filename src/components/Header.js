import React from 'react';
import HeaderNavigatorBar from "./header/HeaderNavigatorBar";
import HeaderSearchBar from "./header/HeaderSearchBar";
import HeaderCategoryBar from "./header/HeaderCategoryBar";

function Header(props) {
    return (
        <>
            <HeaderNavigatorBar/>
            <HeaderSearchBar/>
            <HeaderCategoryBar/>
        </>
    );
}

export default Header;