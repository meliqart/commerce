import React, {useState} from 'react';
import PRODUCT from "../data/products.json"
import ProductItem from "./ProductItem";
import {useSelector} from "react-redux";
import _ from "lodash"


function ProductList(props) {
    const [height, setHeight] = useState(80);
    const [more, setMore] = useState("more");
    const [show, setShow] = useState(true);
    const state = useSelector((store) => store.searchData.searchData)
    const idData = useSelector((store) => store.categoryId.id)
    const priceData = useSelector((store) => store.priceData.data)
    const filters = useSelector((store) => store.filters.filters)
    const rating = useSelector((store) => store.filters.rating)


    const handleClick = () => {
        if (show === true) {
            setHeight(60)
            setMore("less")
            setShow(!true)
            return
        }
        setHeight(80)
        setMore("more")
        setShow(true)

    }
    let data = _.clone(PRODUCT)

    const renderProduct = (productID) => {
        if (state !== '') {
            data = data.filter((data => data.Name.toLowerCase().includes(state)))
        }
        if (productID.length) {
            productID.map((value) => {
                data = data.filter((product) => product.categoryId.id.includes(value))
            })
        }
        if (priceData) {
            data = data.filter((product) => priceData[0] < product.original_price && priceData[1] > product.original_price)
        }
        if (filters) {
            filters.map((value) => {
                data = data.filter((product) => +product[`Bulb_and_PowerID`].id.includes(+value))
            })
        }
        if (rating) {
            rating.map((value) => {
                data = data.filter((product) => product.rating.includes(value))
            })
        }
        return data.map((product) => {
            return <ProductItem key={product.id} product={product}/>
        })
    }


    return (
        <div className="product_block">
            <h1 className="product_block_name">Title of listing page</h1>
            <div className="aaa">
                <p className="product_block_txt" style={{height: `${height}px`}}>Single row angular contact bearings are
                    capable of taking radial loads, as well as axial loads in one direction. They <br/> are able to
                    transmit radial and axial forces simultaneously. These bearings are an excellent choice for high
                    speed,<br/> low load applications. They have a low coefficient of friction and run an</p>
            </div>
            <button onClick={() => handleClick()} className="product_block_button">Show {more}</button>
            <div className="product_block_banner">
                <img className="product_block_banner_img" src="image5462.png" alt=""/>
                <p className="product_block_banner_txt"><span className="product_block_banner_procent">-20%</span> <span
                    className="product_block_banner_procent_txt">on power tools</span></p>
                <button className="product_block_banner__button"><span
                    className="product_block_banner__txt">Check offer</span></button>
            </div>
            <div className="product_items">
                {renderProduct(idData)}
            </div>


        </div>
    );
}

export default ProductList;