import React from 'react';
import Filters from "./Filters";
import ProductList from "./ProductList";

function Header(props) {

    return (
        <div className="main">
            <div className="container">
                <div className="main_block">
                    <Filters/>
                    <ProductList/>
                </div>
            </div>
        </div>
    );
}

export default Header;