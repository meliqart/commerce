import React, {useState} from 'react';
import FILTERS from "../../data/filtersType.json"
import {useDispatch} from "react-redux";
import {Link} from "react-router-dom";
import {filtersChanged} from "../../store/action/FiltersChenged";


function FiltersList(props) {
    const [idValue, setIdValue] = useState([])
    const dispatch = useDispatch()

    const handleChange = (categoryId) => {
        let idValueNew = [...idValue]
        const i = idValue.indexOf(categoryId);
        if (i > -1) {
            getChildrenId(categoryId).forEach(id => {
                idValueNew = idValueNew.filter(v => v !== id)
            })
            idValueNew.splice(i, 1)
        } else {
            idValueNew.push(categoryId)
        }
        setIdValue(idValueNew)

        dispatch(filtersChanged(idValueNew))
    }


    const renderItems = (parentId) => {
        return FILTERS.filter(cat => cat.parentId === parentId).map(cat => (
            <li key={cat.id} className="collapsed">
                <label className="item">
                    <input type="checkbox" checked={idValue.includes(cat.id)}
                           onChange={() => handleChange(cat.id)}/>
                    <Link className="item_1" to="#">{cat.title}</Link>
                </label>
                {idValue.includes(cat.id) ? (
                    <ul className="sub-menu collapse show">
                        {renderItems(cat.id)}
                    </ul>
                ) : null}
            </li>
        ))
    }

    const getChildrenId = (parentId) => {
        let list = [];

        function _getChildrenId(pId) {
            FILTERS.forEach((cat) => {
                if (cat.parentId === pId) {
                    list.push(cat.id);
                    _getChildrenId(cat.id);
                }
            })
        }

        _getChildrenId(parentId)

        return list;
    }


    return (
        <div className="main_category">
            <h3>Filters</h3>
            <div className="menu-list">
                <ul id="menu-content2" className="menu-content collapse out">
                    {renderItems()}
                </ul>
            </div>
        </div>
    );
}

export default FiltersList;