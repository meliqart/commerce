import React, {useState} from 'react';
import _ from "lodash"
import {AiOutlineStar, AiTwotoneStar} from "react-icons/ai";
import { ratingChanged} from "../../store/action/FiltersChenged";
import {useDispatch} from "react-redux";

function RatingFilter(props) {
    const [starData, setStartData] = useState([])
    const dispatch = useDispatch()

    const handleRating = (ev) => {
        let starDataNew = [...starData]
        const i = starData.indexOf(ev.target.id);
        if (i > -1) {
            starDataNew.splice(i, 1)
        } else {
            starDataNew.push(ev.target.id)
        }
        setStartData(starDataNew)

        dispatch(ratingChanged(starDataNew))
    }
    return (
        <div className="rating_block">
            <h3 className="menu-tittle-brands">Rating</h3>
            <ul>
                {_.range(0, 5).map((p) => (
                    <li key={p + 66}>
                        <form>
                            <input onChange={(ev) => handleRating(ev)} className="menu-checkbox" id={5 - p}
                                   type="checkbox"/>
                            <label className="menu-categories-label" htmlFor={5 - p}>
                                {_.range(0, 5 - p).map((n) => (
                                    <AiTwotoneStar key = {n} color={"#FDBC15"}/>
                                ))}
                                {_.range(0, p).map((n) => (
                                    <AiOutlineStar key = {n} color={"#D1D1D1"}/>
                                ))}
                            </label>
                        </form>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default RatingFilter;