import React, {useState} from 'react';
import CATEGORIES from "../../data/categories.json"
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {categoryChanged} from "../../store/action/CategoryChanged"


function CategoryFilter(props) {
    const [idValue, setIdValue] = useState([])
    const dispatch = useDispatch()

    const handleChange = (categoryId) => {
        let idValueNew = [...idValue]
        const i = idValue.indexOf(categoryId);
        if (i > -1) {
            getChildrenId(categoryId).forEach(id => {
                idValueNew = idValueNew.filter(v => v !== id)
            })
            idValueNew.splice(i, 1)
        } else {
            idValueNew.push(categoryId)
        }
        setIdValue(idValueNew)

        dispatch(categoryChanged(idValueNew))
    }


    const renderItems = (parentId) => {
        return CATEGORIES.filter(cat => cat.parentId === parentId).map(cat => (
            <li key={cat.id} className="collapsed">
                <label className="item">
                    <input type="checkbox" checked={idValue.includes(cat.id)}
                           onChange={() => handleChange(cat.id)}/>
                    <Link className="item_1" to="#">{cat.title}</Link>
                </label>
                {idValue.includes(cat.id) ? (
                    <ul className="sub-menu collapse show">
                        {renderItems(cat.id)}
                    </ul>
                ) : null}
            </li>
        ))
    }

    const getChildrenId = (parentId) => {
        let list = [];

        function _getChildrenId(pId) {
            CATEGORIES.forEach((cat) => {
                if (cat.parentId === pId) {
                    list.push(cat.id);
                    _getChildrenId(cat.id);
                }
            })
        }

        _getChildrenId(parentId)

        return list;
    }


    return (
        <div className="main_category">
            <h3>Subcategories</h3>
            <div className="menu-list">
                <ul id="menu-content2" className="menu-content collapse out">
                    {renderItems()}
                </ul>
            </div>
        </div>
    );
}

export default CategoryFilter;