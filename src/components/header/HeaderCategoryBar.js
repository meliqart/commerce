import React from 'react';
import {Link} from "react-router-dom";

function HeaderCategoryBar(props) {
    return (
        <div className="category_bar">
            <div className="container">
                <div className="category_bar_block">
                    <div className="category_bar_select">
                        <select className="category_bar_selects" name="" id="">
                            <option value="">HOME & GARDEN</option>
                        </select>
                        <select className="category_bar_selects" name="" id="">
                            <option value="">MOTORS</option>
                        </select>
                        <Link className="category_bar_link" to="#">ELECTRONICS</Link>
                        <Link className="category_bar_link" to="#">OFFICE EQUIPMENT</Link>
                    </div>
                    <div className="category_bar_CFC">
                        <figure>
                            <img src="cfc/compare.png" alt="compare"/>
                            <figcaption>
                                <span className="compare_txt">COMPARE</span>
                            </figcaption>
                        </figure>
                        <figure>
                            <img src="cfc/star.png" alt="compare"/>
                            <figcaption>
                                <span>FAVORITE</span>
                            </figcaption>
                        </figure>
                        <figure>
                            <img src="cfc/Vector.png" alt="compare"/>
                            <figcaption>
                                <span>Active Cart Name</span>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HeaderCategoryBar;