import React, {useEffect, useState} from 'react';
import CountryesLenguage from "../../data/countryes_lenguage.json"
import {BsFillTelephoneForwardFill} from 'react-icons/bs'
import {AiFillMessage} from 'react-icons/ai'
import {BiUserCircle} from "react-icons/bi"
import {Link} from "react-router-dom";

function HeaderNavigatorBar(props) {
    const data = CountryesLenguage;

    const handleSubmit = (e) => {
        e.preventDefault()
    }
    useEffect(() => {

    }, [])

    return (
        <div id="header">
            <div className="container">
                <div className="header">
                    <div className="header_form">
                        <form onSubmit={e => handleSubmit(e)} action="#" className="header_form_1">
                            <label htmlFor="country"><span className="header_form_1_txt">Country: </span></label>
                            <select id='country' name='country'>
                                {
                                    data ? data.map((d) => {
                                        return <option key={d.id}>{d.name}</option>
                                    }) : null
                                }
                            </select>
                            <label htmlFor="language"><span className="header_form_1_txt">Language: </span></label>
                            <select id="language" name='country'>
                                {
                                    data ? data.map((d) => {
                                        return <option key={d.id}>{d.languages[0]}</option>
                                    }) : null
                                }
                            </select>
                            <label htmlFor="currency"><span className="header_form_1_txt">Currency: </span></label>
                            <select id="currency" name='country'>
                                {
                                    data ? data.map((d) => {
                                        return <option key={d.id}>{d.currency}</option>
                                    }) : null
                                }
                            </select>
                        </form>
                    </div>
                    <div className="user_data">
                        <div className="user_data_phone"><BsFillTelephoneForwardFill className="phone_icon"/><a
                            href="tel:+32(0)15287667">
                            +32 (0) 15 28 76 67</a></div>
                        <div className="user_data_messages"><Link to="#"><AiFillMessage
                            className="messages_icon"/> Messages</Link></div>
                        <div className="user_data_user">
                            <BiUserCircle className="user_data_icon"/>
                            <select className="user_data_select" name="" id="">
                                <option value="">Username Surname</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    );
}

export default HeaderNavigatorBar;