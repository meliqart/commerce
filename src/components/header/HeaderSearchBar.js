import React, {useState} from 'react';
import {AiOutlineSearch} from "react-icons/ai"
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {getMessageData} from "../../store/action/SearchDataAction";

function HeaderSearchBar(props) {
    const [value, setValue] = useState('');
    const dispatch = useDispatch()


    const handleClick = (e) => {
        dispatch(getMessageData(value))
    }
    const handleChange = (e) => {
        e.preventDefault();
        setValue(e.target.value.toLowerCase())
    }
    dispatch(getMessageData(value))
    return (
        <div className="header_search_bar">
            <div className="container">
                <div className="header_search_block">
                    <div className="header_logo">
                        <img src="logo/vector3.png" alt="logo"/>
                        <img style={{width: 180, height: 15}} src="logo/commercebooster.png" alt="logo"/>
                    </div>
                    <div className="search_block">
                        <form className="search_form" action="#">
                            <Link to="#"><AiOutlineSearch className="search_icon"/></Link>
                            <input onChange={(e) => handleChange(e)} className="search_input"
                                   placeholder="Search by product" type="text"/>
                        </form>
                        <button onClick={(e) => handleClick(e)} className="search_button">
                            <span className="search_button_txt">Quick Order Form</span></button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HeaderSearchBar;