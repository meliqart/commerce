import React from 'react';
import {Link} from "react-router-dom";


function ProductItem(props) {
    const {product} = props


    return (
        <div className="item_block">
            <div className="item_block_block">
                <img className="item_block_img" src={product.img} alt="photo"/>
            </div>
            <Link to="#" className="item_block_brandName">{product.brandName}</Link> <br/>
            <Link to="#"  className="item_block_Name">{product.Name}</Link><br/>
            <Link to ="#" className="item_block_type">Bulb_thread_type: {product.Bulb_thread_type}</Link><br/>
            <Link to = "#" className="item_block_Max_power">Max_power: {product.Max_power}</Link><br/>
            <Link to = "#" className="item_block_Luminous_flux">Luminous_flux: {product.Luminous_flux}</Link><br/>
            <Link to = "#" className="item_block_your_price">your_price <span className="item_block_your_price_span">{product.your_price}</span>  net</Link>
            <Link to = "#" className="item_block_price">{product.price}</Link><br/>
            <button className="item_button"><span className="item_button_txt_txt">Add to cart</span></button>
        </div>
    );
}

export default ProductItem;