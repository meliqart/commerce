import React from 'react';
import CategoryFilter from "./filters/CategoryFilter";
import RatingFilter from "./filters/RatingFilter";
import PriceFilter from "./filters/PriceFilter";
import FiltersList from "./filters/FiltersList";

function Filters(props) {

    return (
        <div className="filter">
            <CategoryFilter />
            <FiltersList/>
            <PriceFilter/>
            <RatingFilter/>
        </div>
    );
}

export default Filters;